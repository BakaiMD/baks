/*!C���� ����� ���������
*/
#pragma once
#include <windows.h>
#include <locale>
#include <iostream>
#include <string.h>
#include <list>
#include <stdio.h>

using  std::endl;
using  std::cout;
using  std::cin;
using  std::list;

///��� ������
enum v_type
{
	normal,
	monetary,
	forward,
	type_count
};

char	*typeStr[type_count] =
{
	"�������",
	"��������",
	"�������"
};

struct	VDATE
{
	unsigned char	day;
	unsigned char	month;
	unsigned short	year;
};
/*! ����� ����� ����� */
/// ����� ����������� ������ ������������ ��� �������� � ��������� 
class Normal
{
private:
	v_type	_Type;
	double	_Sum;
	double	_SumFirst;
	VDATE	*_CreateDate;
	VDATE	*_LastDate;
	double	_Percent;
	bool	_open;
public:
	///����������� ��� ����������� ������ � ��������� ����� ,�������� � ���� (���������� �����)
	Normal(double sum, double percent, v_type type = normal)
	{
		_Type = type;
		_Sum = sum;
		_SumFirst = sum;
		_CreateDate = GetCurrentDate();
		_LastDate = GetCurrentDate();
		_Percent = percent;
		_open = true;
	}///�������� ����������� ������
	~Normal()
	{
		delete _CreateDate;
		delete _LastDate;
	}

	friend class Monetary;
	friend class Forward;
	inline v_type GetType() { return _Type; }
	inline double GetSum() { return _Sum; }
	inline double GetSumPercent() { return (_Sum - _SumFirst); }
	///����� ��� ��������� ����� 
	inline void SetSum(double sum) { _Sum = sum; }
	inline VDATE* GetCreateDate() { return _CreateDate; }
	inline VDATE* GetLastDate() { return _LastDate; }
	///����� ��� ��������� ���� ������������� ������ 
	inline void SetLastDate(VDATE *date) { *_LastDate = *date; }
	inline double GetPercent() { return _Percent; }
	///����� ��� �������� ������
	inline bool IsOpen() { return _open; }
	///����� ��� �������� ������ ������� ���� 
	virtual void Close(VDATE *pdate)
	{
		if (_open)
		{
			CalcVklad(pdate);
			_open = false;
		}
	}
	///����� ��� ��������� ���� � ���������� ���������� ��������� ����
	VDATE* GetCurrentDate()
	{
		SYSTEMTIME	st;
		VDATE *dt = new VDATE;
		GetLocalTime(&st);
		dt->day = (unsigned char)st.wDay;
		dt->month = (unsigned char)st.wMonth;
		dt->year = (unsigned short)st.wYear;
		return dt;
	}
	///����� ��� ������� ����� � ���������.���������� ���� ���� ������,������� ������ ����
	virtual void CalcVklad(VDATE *pdate)
	{
		try
		{
			int		days = pdate->day - _LastDate->day;
			int		months = pdate->month - _LastDate->month;
			int		years = pdate->year - _LastDate->year;

			if (!IsOpen())
				throw "��� ��������� ����� ���������� �������� ���������� ����������";

			if (days < 0)
			{
				days += 30;
				months--;
			}
			if (months < 0)
			{
				months += 12;
				years--;
			}
			if (years < 0)
				throw "������� ������ ����";

			_Sum += _Sum * (((years * 12 + months) + ((double)days) / 30)) * _Percent / 100;
			_Sum = ((double)((int)(_Sum * 100))) / 100;
			SetLastDate(pdate);
		}
		catch (char * str)
		{
			cout << str << endl;
		}
	}

	bool SetClose(Normal * );
	///����� ���������� ��������� ����� � �������� 
	virtual void View()
	{
		char	str[32];

		cout << endl << "����\t\t";
		if (_open)
			cout << "������";
		else
			cout << "������";
		cout << endl << "��� ������:\t" << typeStr[_Type] << endl;
		cout << "�����:\t\t" << _Sum << endl;
		sprintf_s(str, "%02d.%02d.%04d", _CreateDate->day, _CreateDate->month, _CreateDate->year);
		cout << "���� ��������:\t" << str << endl;
		sprintf_s(str, "%02d.%02d.%04d", _LastDate->day, _LastDate->month, _LastDate->year);
		cout << "���� ���������:\t" << str << endl;
		sprintf_s(str, "%5.2f%%", _Percent);
		cout << "�������:\t" << str << endl;
	};
}; 